﻿namespace TheBigBot
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnInit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMap = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblZone = new System.Windows.Forms.Label();
            this.btnStartFight = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtBoxConsole = new System.Windows.Forms.TextBox();
            this.pictureBoxScreen = new System.Windows.Forms.PictureBox();
            this.grpBxMinage = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBxLevel = new System.Windows.Forms.TextBox();
            this.checkBxLoopUntilPodsFull = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtBoxSortCount = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnChangePlayerInfos = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBoxPm = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBoxRange = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxScreen)).BeginInit();
            this.grpBxMinage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnInit
            // 
            this.btnInit.Location = new System.Drawing.Point(248, 71);
            this.btnInit.Name = "btnInit";
            this.btnInit.Size = new System.Drawing.Size(130, 23);
            this.btnInit.TabIndex = 0;
            this.btnInit.Text = "Lancer le minage";
            this.btnInit.UseVisualStyleBackColor = true;
            this.btnInit.Click += new System.EventHandler(this.btnInit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Map :";
            // 
            // lblMap
            // 
            this.lblMap.AutoSize = true;
            this.lblMap.Location = new System.Drawing.Point(52, 9);
            this.lblMap.Name = "lblMap";
            this.lblMap.Size = new System.Drawing.Size(10, 13);
            this.lblMap.TabIndex = 2;
            this.lblMap.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Zone :";
            // 
            // lblZone
            // 
            this.lblZone.AutoSize = true;
            this.lblZone.Location = new System.Drawing.Point(52, 32);
            this.lblZone.Name = "lblZone";
            this.lblZone.Size = new System.Drawing.Size(10, 13);
            this.lblZone.TabIndex = 4;
            this.lblZone.Text = "-";
            // 
            // btnStartFight
            // 
            this.btnStartFight.Location = new System.Drawing.Point(248, 65);
            this.btnStartFight.Name = "btnStartFight";
            this.btnStartFight.Size = new System.Drawing.Size(130, 23);
            this.btnStartFight.TabIndex = 5;
            this.btnStartFight.Text = "Tester combat";
            this.btnStartFight.UseVisualStyleBackColor = true;
            this.btnStartFight.Click += new System.EventHandler(this.btnStartFight_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtBoxConsole);
            this.panel1.Location = new System.Drawing.Point(552, 163);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(433, 392);
            this.panel1.TabIndex = 6;
            // 
            // txtBoxConsole
            // 
            this.txtBoxConsole.Location = new System.Drawing.Point(4, 3);
            this.txtBoxConsole.Multiline = true;
            this.txtBoxConsole.Name = "txtBoxConsole";
            this.txtBoxConsole.ReadOnly = true;
            this.txtBoxConsole.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBoxConsole.Size = new System.Drawing.Size(424, 384);
            this.txtBoxConsole.TabIndex = 0;
            // 
            // pictureBoxScreen
            // 
            this.pictureBoxScreen.Location = new System.Drawing.Point(12, 163);
            this.pictureBoxScreen.Name = "pictureBoxScreen";
            this.pictureBoxScreen.Size = new System.Drawing.Size(534, 392);
            this.pictureBoxScreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxScreen.TabIndex = 7;
            this.pictureBoxScreen.TabStop = false;
            // 
            // grpBxMinage
            // 
            this.grpBxMinage.Controls.Add(this.label3);
            this.grpBxMinage.Controls.Add(this.txtBxLevel);
            this.grpBxMinage.Controls.Add(this.checkBxLoopUntilPodsFull);
            this.grpBxMinage.Controls.Add(this.btnInit);
            this.grpBxMinage.Location = new System.Drawing.Point(12, 57);
            this.grpBxMinage.Name = "grpBxMinage";
            this.grpBxMinage.Size = new System.Drawing.Size(384, 100);
            this.grpBxMinage.TabIndex = 8;
            this.grpBxMinage.TabStop = false;
            this.grpBxMinage.Text = "Options du minage";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Mineur niveau ";
            // 
            // txtBxLevel
            // 
            this.txtBxLevel.Location = new System.Drawing.Point(90, 22);
            this.txtBxLevel.MaxLength = 3;
            this.txtBxLevel.Name = "txtBxLevel";
            this.txtBxLevel.Size = new System.Drawing.Size(33, 20);
            this.txtBxLevel.TabIndex = 10;
            this.txtBxLevel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBxLevel_KeyPress);
            // 
            // checkBxLoopUntilPodsFull
            // 
            this.checkBxLoopUntilPodsFull.AutoSize = true;
            this.checkBxLoopUntilPodsFull.Location = new System.Drawing.Point(6, 71);
            this.checkBxLoopUntilPodsFull.Name = "checkBxLoopUntilPodsFull";
            this.checkBxLoopUntilPodsFull.Size = new System.Drawing.Size(185, 17);
            this.checkBxLoopUntilPodsFull.TabIndex = 9;
            this.checkBxLoopUntilPodsFull.Text = "Continuer jusqu\'à l\'inventaire plein";
            this.checkBxLoopUntilPodsFull.UseVisualStyleBackColor = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtBoxRange);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtBoxSortCount);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnChangePlayerInfos);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtBoxPm);
            this.groupBox1.Controls.Add(this.btnStartFight);
            this.groupBox1.Location = new System.Drawing.Point(601, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 100);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Options des combats";
            // 
            // txtBoxSortCount
            // 
            this.txtBoxSortCount.Location = new System.Drawing.Point(116, 52);
            this.txtBoxSortCount.MaxLength = 3;
            this.txtBoxSortCount.Name = "txtBoxSortCount";
            this.txtBoxSortCount.Size = new System.Drawing.Size(33, 20);
            this.txtBoxSortCount.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Nombre de lancé";
            // 
            // btnChangePlayerInfos
            // 
            this.btnChangePlayerInfos.Location = new System.Drawing.Point(248, 15);
            this.btnChangePlayerInfos.Name = "btnChangePlayerInfos";
            this.btnChangePlayerInfos.Size = new System.Drawing.Size(130, 23);
            this.btnChangePlayerInfos.TabIndex = 12;
            this.btnChangePlayerInfos.Text = "Appliquer";
            this.btnChangePlayerInfos.UseVisualStyleBackColor = true;
            this.btnChangePlayerInfos.Click += new System.EventHandler(this.btnChangePlayerInfos_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Nombre de PM";
            // 
            // txtBoxPm
            // 
            this.txtBoxPm.Location = new System.Drawing.Point(116, 22);
            this.txtBoxPm.MaxLength = 3;
            this.txtBoxPm.Name = "txtBoxPm";
            this.txtBoxPm.Size = new System.Drawing.Size(33, 20);
            this.txtBoxPm.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Portée du sort";
            // 
            // txtBoxRange
            // 
            this.txtBoxRange.Location = new System.Drawing.Point(116, 78);
            this.txtBoxRange.MaxLength = 3;
            this.txtBoxRange.Name = "txtBoxRange";
            this.txtBoxRange.Size = new System.Drawing.Size(33, 20);
            this.txtBoxRange.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 579);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpBxMinage);
            this.Controls.Add(this.pictureBoxScreen);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblZone);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblMap);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxScreen)).EndInit();
            this.grpBxMinage.ResumeLayout(false);
            this.grpBxMinage.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnInit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMap;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblZone;
        private System.Windows.Forms.Button btnStartFight;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxScreen;
        private System.Windows.Forms.TextBox txtBoxConsole;
        private System.Windows.Forms.GroupBox grpBxMinage;
        private System.Windows.Forms.CheckBox checkBxLoopUntilPodsFull;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBxLevel;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBoxPm;
        private System.Windows.Forms.Button btnChangePlayerInfos;
        private System.Windows.Forms.TextBox txtBoxSortCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBoxRange;
        private System.Windows.Forms.Label label6;
    }
}

