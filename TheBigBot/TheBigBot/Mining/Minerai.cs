﻿using System.Drawing;


namespace TheBigBot
{
    class Minerai
    {
        private Point position { get; set; }
        private MineraiType type { get; set; }

        public Minerai(Point position, MineraiType type)
        {
            this.position = position;
            this.type = type;
        }

        public Point GetPosition()
        {
            return this.position;
        }

        public MineraiType GetMineraiType()
        {
            return this.type;
        }
    }
            
}
