﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheBigBot
{
    enum MineraiType
    {
        Fer = 0,
        Cuivre = 20,
        Bronze = 40,
        Kobalte = 60,
        Manganèse = 80,
        Etain = 100,
        Silicate = 100,
        Argent = 120,
        Bauxite = 140,
        Or = 160,
        Dolomite = 180,
        Obsidienne = 200,
        Ecume = 200
    }
}
