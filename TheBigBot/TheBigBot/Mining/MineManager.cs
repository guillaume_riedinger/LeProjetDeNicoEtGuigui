﻿using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheBigBot.Mining
{
    static class MineManager
    {
        public static List<Mine> mines= new List<Mine>();

        public static void InstantiateMines()
        {
            List<Minerai> astrub1Minerais = new List<Minerai>();
            astrub1Minerais.Add(new Minerai(new Point(412, 611), MineraiType.Fer));
            astrub1Minerais.Add(new Minerai(new Point(509, 634), MineraiType.Cuivre));
            astrub1Minerais.Add(new Minerai(new Point(660, 515), MineraiType.Fer));
            astrub1Minerais.Add(new Minerai(new Point(697, 459), MineraiType.Fer));
            astrub1Minerais.Add(new Minerai(new Point(980, 393), MineraiType.Cuivre));
            astrub1Minerais.Add(new Minerai(new Point(1021, 374), MineraiType.Fer));
            astrub1Minerais.Add(new Minerai(new Point(1137, 302), MineraiType.Bronze));
            astrub1Minerais.Add(new Minerai(new Point(1288, 430), MineraiType.Fer));

            List<Minerai> astrub2Minerais = new List<Minerai>();
            astrub2Minerais.Add(new Minerai(new Point(477, 385), MineraiType.Cuivre));
            astrub2Minerais.Add(new Minerai(new Point(510, 399), MineraiType.Fer));
            astrub2Minerais.Add(new Minerai(new Point(596, 353), MineraiType.Bronze));
            astrub2Minerais.Add(new Minerai(new Point(642, 388), MineraiType.Fer));
            astrub2Minerais.Add(new Minerai(new Point(928, 247), MineraiType.Fer));
            astrub2Minerais.Add(new Minerai(new Point(1177, 228), MineraiType.Fer));
            astrub2Minerais.Add(new Minerai(new Point(1256, 214), MineraiType.Fer));
            astrub2Minerais.Add(new Minerai(new Point(1346, 290), MineraiType.Etain));
            astrub2Minerais.Add(new Minerai(new Point(1401, 309), MineraiType.Fer));
            astrub2Minerais.Add(new Minerai(new Point(1082, 502), MineraiType.Fer));
            astrub2Minerais.Add(new Minerai(new Point(1131, 472), MineraiType.Bronze));
            

            List<Minerai> astrub3Minerais = new List<Minerai>();
            astrub3Minerais.Add(new Minerai(new Point(816, 237), MineraiType.Silicate));
            astrub3Minerais.Add(new Minerai(new Point(1010, 280), MineraiType.Fer));
            astrub3Minerais.Add(new Minerai(new Point(1041, 266), MineraiType.Cuivre));
            astrub3Minerais.Add(new Minerai(new Point(690, 526), MineraiType.Fer));
            astrub3Minerais.Add(new Minerai(new Point(918, 438), MineraiType.Fer));
            astrub3Minerais.Add(new Minerai(new Point(1268, 390), MineraiType.Cuivre));

            List<Map> maps = new List<Map>();
            Map astrub1 = new Map(1, -17, astrub1Minerais);
            Map astrub2 = new Map(2, -18, astrub2Minerais);
            Map astrub3 = new Map(3, -18, astrub3Minerais);
            astrub1.AddSortie(new Exit(new Point(332, 886), null));
            astrub1.AddSortie(new Exit(new Point(1541, 302), astrub2));

            astrub2.AddSortie(new Exit(new Point(347, 778), astrub1));
            astrub2.AddSortie(new Exit(new Point(1503, 586), astrub3));

            astrub3.AddSortie(new Exit(new Point(506, 259), astrub2));

            maps.Add(astrub1);
            maps.Add(astrub2);
            maps.Add(astrub3);

            Mine astrub = new Mine(1, -17, maps, new Point(1140, 633));
            mines.Add(astrub);

        }

    }
}
