﻿using System.Drawing;
using System.Collections.Generic;

namespace TheBigBot.Mining
{
    class Mine
    {
        private int x { get; set; }
        private int y { get; set; }
        private List<Map> maps { get; set; }
        private Point entrancePosition { get; set; }

        public Mine(int x, int y, List<Map> maps, Point entrancePosition)
        {
            this.x = x;
            this.y = y;
            this.maps = maps;
            this.entrancePosition = entrancePosition;
        }

        public int GetX()
        {
            return this.x;
        }

        public int GetY()
        {
            return this.y;
        }

        public List<Map> GetMaps()
        {
            return this.maps;
        }

        public Point GetEntrancePosition()
        {
            return this.entrancePosition;
        }




    }
}
