﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheBigBot.Pathfinding
{
    public class PathfindingCell
    {
        public int X;
        public int Y;

        public PathfindingCell(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return X + ", " + Y;
        }
    }
}
