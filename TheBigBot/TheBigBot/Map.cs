﻿using System.Collections.Generic;

namespace TheBigBot
{
    class Map
    {
        private int x { get; set; }
        private int y { get; set; }
        private List<Minerai> minerais { get; set; }
        private List<Exit> sorties { get; set; }

        public Map(int x, int y, List<Minerai> minerais)
        {
            this.x = x;
            this.y = y;
            this.minerais = minerais;
            this.sorties = new List<Exit>();
        }

        public int GetX()
        {
            return this.x;
        }

        public int GetY()
        {
            return this.y;
        }

        public List<Minerai> GetMinerais()
        {
            return this.minerais;
        }

        public List<Exit> GetSorties()
        {
            return this.sorties;
        }

        public void AddSortie(Exit exit)
        {
            this.sorties.Add(exit);
        }
    }
}
