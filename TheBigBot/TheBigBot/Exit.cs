﻿using System.Drawing;

namespace TheBigBot
{
    class Exit
    {
        private Point position { get; set; }
        private Map map;

        public Exit(Point position, Map map)
        {
            this.position = position;
            this.map = map;
        }

        public Point GetPosition()
        {
            return this.position;
        }

        public Map GetMap()
        {
            return this.map;
        }

        public void SetMap(Map map)
        {
            this.map = map;
        }
    }
}
