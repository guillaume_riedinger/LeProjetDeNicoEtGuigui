﻿using IronOcr;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheBigBot
{
    static class PlayerManager
    {

        public static bool IsPodbarFull()
        {
            Bitmap bitMap = BitMapGetter.GetPodBar();
            Color c = bitMap.GetPixel(468, 4);
            return c.GetBrightness() != 0;
        }

        public static bool CanMine()
        {
            Bitmap bp = BitMapGetter.GetTooltip();
            Pixel p = MapManager.GetPixelWithColor(Color.FromArgb(247, 204, 0), bp);
            return p == null;
        }
    }
}
