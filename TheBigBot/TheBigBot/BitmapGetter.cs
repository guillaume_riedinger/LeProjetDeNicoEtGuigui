﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TheBigBot
{
    public class BitMapGetter
    {
        public static Bitmap GetCoordonnées()
        {
            Bitmap bitMap = new Bitmap(300,
                               100,
                               PixelFormat.Format32bppArgb);
            var gfxScreenshot = Graphics.FromImage(bitMap);

            gfxScreenshot.CopyFromScreen(0,
                            30,
                            0,
                            0,
                            bitMap.Size,
                            CopyPixelOperation.SourceCopy);

            return bitMap;
        }

        public static Bitmap GetPodBar()
        {
            Bitmap bitMap = new Bitmap(474,
                   9,
                   PixelFormat.Format32bppArgb);
            var gfxScreenshot = Graphics.FromImage(bitMap);

            gfxScreenshot.CopyFromScreen(834,
                            1027,
                            0,
                            0,
                            bitMap.Size,
                            CopyPixelOperation.SourceCopy);

            return bitMap;
        }

        public static Bitmap GetTooltip()
        {
            Point position = MouseManager.GetCursorPosition();
            Bitmap bitMap = new Bitmap(350,
                   250,
                   PixelFormat.Format32bppArgb);
            var gfxScreenshot = Graphics.FromImage(bitMap);

            gfxScreenshot.CopyFromScreen(position.X,
                            position.Y - 175,
                            0,
                            0,
                            bitMap.Size,
                            CopyPixelOperation.SourceCopy);

            return bitMap;
        }

        public static bool CompareTwoBitmapPixels(Bitmap bmp1,Bitmap bmp2)
        {
            int bytes = bmp1.Width * bmp1.Height * (Image.GetPixelFormatSize(bmp1.PixelFormat) / 8);
            bool result = true;
            byte[] b1bytes = new byte[bytes];
            byte[] b2bytes = new byte[bytes];

            BitmapData bitmapData1 = bmp1.LockBits(new Rectangle(0, 0, bmp1.Width, bmp1.Height), ImageLockMode.ReadOnly, bmp1.PixelFormat);
            BitmapData bitmapData2 = bmp2.LockBits(new Rectangle(0, 0, bmp2.Width, bmp2.Height), ImageLockMode.ReadOnly, bmp2.PixelFormat);

            Marshal.Copy(bitmapData1.Scan0, b1bytes, 0, bytes);
            Marshal.Copy(bitmapData2.Scan0, b2bytes, 0, bytes);

            for (int n = 0; n <= bytes - 1; n++)
            {
                if (b1bytes[n] != b2bytes[n])
                {
                    result = false;
                    break;
                }
            }

            bmp1.UnlockBits(bitmapData1);
            bmp2.UnlockBits(bitmapData2);

            return result;
        }
    }


}
