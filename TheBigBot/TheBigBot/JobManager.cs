﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheBigBot.Mining;
using System.Drawing;

namespace TheBigBot
{
    static class JobManager
    {
        private static Stack<Map> exploredMaps = new Stack<Map>();
        private static List<Exit> exploredSorties = new List<Exit>(); 
        private static Mine currentMine = null;

        private const int TIME_TO_MINE = 7000;
        private const int TIME_FOR_MAP_LOAD = 6000;


        public static void StartMining(Mine mine, bool keepGoingUntilPodsFull, int levelThreshold)
        {
            currentMine = mine;
            if (PlayerManager.IsPodbarFull()) throw new Exception("Plus de place dans l'inventaire ! Videz vos pods");

            WindowsManager.Log("Lancement du minage");
            MapManager.GetMapInformations();
            if (MapInformations.X == mine.GetX() && MapInformations.Y == mine.GetY())
            {
                Map entranceMap = mine.GetMaps()[0];
                MouseManager.Click(mine.GetEntrancePosition());
                while(!keepGoingUntilPodsFull || (keepGoingUntilPodsFull && !PlayerManager.IsPodbarFull()))
                {
                    exploredMaps = new Stack<Map>();
                    exploredMaps.Push(entranceMap);
                    exploredSorties = new List<Exit>();
                    do
                    {
                        Map currentMap = exploredMaps.Peek();
                        WindowsManager.Log("Carte actuelle : " + currentMap.GetX() + "," + currentMap.GetY() + " - " + currentMap.GetMinerais().Count + " minerais potentiels à récupérer ici.");
                        Thread.Sleep(TIME_FOR_MAP_LOAD);
                        foreach (Minerai minerai in currentMap.GetMinerais())
                        {
                            if( (int) minerai.GetMineraiType() < levelThreshold)
                            {
                                MouseManager.MoveMouse(minerai.GetPosition());
                                Thread.Sleep(100);
                                if(PlayerManager.CanMine())
                                {
                                    MouseManager.ShiftClick();
                                    Thread.Sleep(TIME_TO_MINE);
                                }
                                if (PlayerManager.IsPodbarFull()) throw new Exception("Plus de place dans l'inventaire ! Videz vos pods");
                            }
                        }
                    } while (GoToNextMap());
                }
                WindowsManager.Log("Fin de l'exploration");
            }
            else
            {
                throw new Exception("Le personnage ne se trouve pas aux coordonnées de la mine.");
            }
        }

        public static Exit FindNextUnexploredExit()
        {
            if (exploredMaps.Count == 0) return null;
            Map currentMap = exploredMaps.Peek();
            List<Exit> unexploredExits = new List<Exit>(currentMap.GetSorties());
            foreach (Exit exit in currentMap.GetSorties())
            {
                if(exploredSorties.Contains(exit))
                {
                    unexploredExits.Remove(exit);
                }
            }
            if (unexploredExits.Count > 1)
            {
                return unexploredExits[1];
            }
            else
            {
                currentMap = exploredMaps.Pop();
                if(currentMap.GetSorties()[0].GetMap() == null)
                {
                    exploredMaps = new Stack<Map>();
                    exploredMaps.Push(currentMine.GetMaps()[0]);
                    exploredSorties = new List<Exit>();
                    return currentMine.GetMaps()[0].GetSorties()[1];
                }
                return currentMap.GetSorties()[0];
            }
        }

        public static bool GoToNextMap()
        {
            Exit nextExit = FindNextUnexploredExit();
            Map currentMap = exploredMaps.Peek();
            if(currentMap == null)
            {
                WindowsManager.Log("Vous êtes de retour à l'entrée");

                exploredMaps = new Stack<Map>();
                exploredMaps.Push(currentMine.GetMaps()[0]);
                exploredSorties = new List<Exit>();
                return true;
            }
            if(exploredMaps.Count == 0)
            {
                WindowsManager.Log("Vous êtes retourné à l'entrée");
                return false;
            }
            else
            {
                WindowsManager.Log("Sortie trouvée, clic en " + nextExit.GetPosition());
                MouseManager.Click(nextExit.GetPosition());
                exploredMaps.Push(nextExit.GetMap());
                exploredSorties.Add(nextExit);
            }
            return true;


        }

        public static void GoBackToMineEntrance()
        {
            while(!(exploredMaps.Count == 0))
            {
                Map currentMap = exploredMaps.Pop();
                MouseManager.Click(currentMap.GetSorties()[0].GetPosition());
                Thread.Sleep(3000);
            }
        }



    }
}
