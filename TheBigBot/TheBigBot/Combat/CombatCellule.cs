﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheBigBot.Pathfinding;

namespace TheBigBot.Combat
{
    public class CombatCellule
    {
        public int PixelX;
        public int PixelY;
        public int CellX;
        public int CellY;
        public Color Color;
        public bool HasEnnemy;
        public bool isWalkable;
        public bool HasJoueur;

        public PathfindingCell pathfindingCell;
    }
}
