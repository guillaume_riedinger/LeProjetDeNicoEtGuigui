﻿using RoyT.AStar;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TheBigBot.Combat;
using TheBigBot.Pathfinding;

namespace TheBigBot.Combat
{
    public static class CombatManager
    {
        public static CombatMap Map;
        public static void InitTurn()
        {
            GetMap();
            Grid grid = GetGrid();
            DoMovement(Map.GetPathToClosestEnnemy(grid));
            Thread.Sleep(500);
            LancerSort();
            if (MustFinishFight())
            {
                return;
            }
            else
            {
                PassTurn();
                WaitForNextTurn();
            }
        }

        public static bool MustFinishFight()
        {
            Bitmap bitMap = new Bitmap(10,
                   10,
                   PixelFormat.Format32bppArgb);
            var gfxScreenshot = Graphics.FromImage(bitMap);

            gfxScreenshot.CopyFromScreen(1340,
                            950,
                            0,
                            0,
                            bitMap.Size,
                            CopyPixelOperation.SourceCopy);

            if (MapManager.GetPixelWithColor(Color.FromArgb(65, 64, 44), bitMap) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void WaitForNextTurn()
        {
            while (true)
            {
                Thread.Sleep(500);

                Bitmap bitMap = new Bitmap(10,
                   10,
                   PixelFormat.Format32bppArgb);
                var gfxScreenshot = Graphics.FromImage(bitMap);

                gfxScreenshot.CopyFromScreen(1350,
                                970,
                                0,
                                0,
                                bitMap.Size,
                                CopyPixelOperation.SourceCopy);

                if (MapManager.GetPixelWithColor(Color.FromArgb(206, 240, 0), bitMap) != null)
                {
                    bitMap.Dispose();
                    InitTurn();
                    return;
                }
            }
        }

        public static void DoMovement(Position[] path)
        {
            if (path.Length > 2)
            {
                CombatCellule cell;
                if (path.Length < Player.PM + 1)
                {
                    cell = Map.GetCellFromPosition(path[path.Length - 2].X, path[path.Length - 2].Y);
                }
                else
                {
                    cell = Map.GetCellFromPosition(path[Player.PM].X, path[Player.PM].Y);
                }

                MouseManager.MoveMouse(cell.PixelX, cell.PixelY);
                WindowsManager.Log("On déplace la souris en " + cell.pathfindingCell.ToString());
                Thread.Sleep(100);
                MouseManager.Click();
                Thread.Sleep(50);
                MouseManager.MoveMouse(100, 600);
            }
        }

        public static void LancerSort()
        {
            GetMap();
            Grid grid = GetGrid();
            Position[] path = Map.GetPathToClosestEnnemy(grid);
            if (path.Length < Player.Range + 1)
            {
                for (int i = 0; i < Player.Lance; i++)
                {
                    MouseManager.MoveMouse(895, 955);
                    Thread.Sleep(100);
                    MouseManager.Click();
                    Thread.Sleep(500);
                    CombatCellule cell = Map.GetCellFromPosition(path[path.Length - 1].X, path[path.Length - 1].Y);
                    MouseManager.MoveMouse(cell.PixelX, cell.PixelY);
                    Thread.Sleep(500);
                    MouseManager.Click();
                    Thread.Sleep(300);
                }
                Thread.Sleep(1500);
            }
        }

        public static void PassTurn()
        {
            MouseManager.MoveMouse(100, 600);
            Thread.Sleep(300);
            MouseManager.MoveMouse(1400, 960);
            Thread.Sleep(100);
            MouseManager.Click();
            MouseManager.MoveMouse(100, 600);
        }

        public static Grid GetGrid()
        {
            Grid grid = new Grid(80, 80);
            for (int i = 0; i < 80; i++)
            {
                for (int n = 0; n < 80; n++)
                {
                    grid.BlockCell(new Position(i, n));
                }
            }

            foreach (PathfindingCell cell in Map.CombatCellules.Where(cells => cells.isWalkable == true).Select(cells => cells.pathfindingCell).ToList())
            {
                grid.UnblockCell(new Position(cell.X, cell.Y));
            }

            grid.UnblockCell(new Position(Map.PlayerPosition.X, Map.PlayerPosition.Y));
            foreach (PathfindingCell cell in Map.ListEnnemies)
            {
                grid.UnblockCell(new Position(cell.X, cell.Y));
            }

            return grid;
        }

        public static void GetMap()
        {
            Map = new CombatMap();
            //foncé : D0E4D8, clair : DAEEE2
            Color[] mapColors = GetWalkableMapColors();
            Map.CombatCellules = GetCellules(mapColors.Select(colors => colors.ToArgb()).ToArray());

            WindowsManager.Log("Map détéctée : " + Map.ListEnnemies.Count + " ennemie(s)");

            DisplayMap(Map);
        }

        public static Color[] GetWalkableMapColors()
        {
            return new Color[] { Color.FromArgb(183, 177, 153), Color.FromArgb(173, 167, 143), Color.FromArgb(104, 141, 86), Color.FromArgb(110, 147, 92) };
        }

        public static List<CombatCellule> GetCellules(int[] isWalkCellsColors)
        {
            Bitmap bitMap = MapManager.GetScreenBitmap();
            List<CombatCellule> res = new List<CombatCellule>();

            for (int n = 0; n < 40; n++)
            {
                for (int i = 0; i < 14; i++)
                {
                    int x = 0;
                    int y = n * 22 + 15;

                    if (n % 2 == 0) //ligne longue
                    {
                        x = i * 89 + 40;
                    }
                    else //ligne courte
                    {
                        x = i * 89 + 85;
                    }

                    int g = 7 / 2;

                    int pathY = n % 2 == 0 ? n / 2 + i : n / 2 + i + 1;
                    int pathX = n / 2 + 40 - i;

                    Color c = bitMap.GetPixel(x, y);

                    CombatCellule cell = new CombatCellule() { Color = c, PixelX = x + 320, PixelY = y + 23, CellX = i, CellY = n, isWalkable = isWalkCellsColors.Contains(c.ToArgb()) };
                    cell.pathfindingCell = new PathfindingCell(pathX, pathY);

                    res.Add(cell);
                }
            }

            FillEnnemiesAndPlayerCell(res, bitMap);


            return res;
        }

        public static void DisplayMap(CombatMap map)
        {
            Bitmap bitMap = MapManager.GetScreenBitmap();
            Graphics g = Graphics.FromImage(bitMap);
            foreach (CombatCellule cell in map.CombatCellules)
            {
                Brush brush;
                if (cell.HasJoueur)
                {
                    brush = Brushes.Blue;
                }
                else if (cell.HasEnnemy)
                {
                    brush = Brushes.Red;
                }
                else
                {
                    brush = cell.isWalkable ? Brushes.White : Brushes.Purple;
                }

                g.DrawString(cell.pathfindingCell.ToString(), new Font("Times New Roman", 12.0f, FontStyle.Regular), brush, cell.PixelX - 15 - 320, cell.PixelY - 23);
            }

            WindowsManager.ChangeScreenImage(bitMap);
        }

        public static void FillEnnemiesAndPlayerCell(List<CombatCellule> combatCellules, Bitmap bitMap)
        {
            Pixel pixelEnnemies = MapManager.GetPixelWithColor(MapManager.EnnemyColor);
            while (pixelEnnemies != null)
            {
                CombatCellule cell = combatCellules.Where(cells => cells.PixelX > pixelEnnemies.X - 20 && cells.PixelX < pixelEnnemies.X + 20 && cells.PixelY > pixelEnnemies.Y - 20 && cells.PixelY < pixelEnnemies.Y + 20).First();
                cell.HasEnnemy = true;
                Map.ListEnnemies.Add(cell.pathfindingCell);

                Graphics g = Graphics.FromImage(bitMap);
                g.FillRectangle(Brushes.White, pixelEnnemies.X - 20 - 320, pixelEnnemies.Y - 20 - 23, 55, 40);
                pixelEnnemies = MapManager.GetPixelWithColor(MapManager.EnnemyColor, bitMap);
            }

            Pixel pixelJoueur = MapManager.GetPixelWithColor(Color.FromArgb(228, 209, 137));

            try
            {
                CombatCellule cell = combatCellules.Where(cells => cells.PixelX > pixelJoueur.X - 20 && cells.PixelX < pixelJoueur.X + 20 && cells.PixelY > pixelJoueur.Y - 20 && cells.PixelY < pixelJoueur.Y + 20).First();
                cell.HasJoueur = true;
                Map.PlayerPosition = cell.pathfindingCell;
            }
            catch (Exception)
            {
                throw new Exception("Le joueur n'est pas trouvé sur la carte de combat. Pensez a activer le mode créature et enlever le mode transparant");
            }

        }
    }
}
