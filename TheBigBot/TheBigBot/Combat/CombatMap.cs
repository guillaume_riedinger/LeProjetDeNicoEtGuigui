﻿using RoyT.AStar;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheBigBot.Pathfinding;

namespace TheBigBot.Combat
{
    public class CombatMap
    {
        public List<CombatCellule> CombatCellules = new List<CombatCellule>();
        public List<PathfindingCell> ListEnnemies = new List<PathfindingCell>();

        public PathfindingCell PlayerPosition { get; set; }

        public Position[] GetPathToClosestEnnemy(Grid grid)
        {
            Position[] path = null;
            foreach (PathfindingCell cellEnnemy in ListEnnemies)
            {
                Position[] newPath = grid.GetPath(new Position(PlayerPosition.X, PlayerPosition.Y), new Position(cellEnnemy.X, cellEnnemy.Y), MovementPatterns.LateralOnly);
                if (newPath.Length == 2)
                {
                    return newPath;
                }
                else
                {
                    if (path == null)
                    {
                        path = newPath;
                    }
                    else
                    {
                        if (newPath.Length < path.Length)
                        {
                            path = newPath;
                        }
                    }
                }
            }

            return path;
        }

        public int GetEnnemyCount()
        {
            return CombatCellules.Where(cells => cells.HasEnnemy == true).ToList().Count;
        }

        public int GetWalkableCells()
        {
            return CombatCellules.Where(cells => cells.isWalkable == true).ToList().Count;
        }

        public int GetUnWalkableCells()
        {
            return CombatCellules.Where(cells => cells.isWalkable == false).ToList().Count;
        }

        public CombatCellule GetCellFromPosition(int x, int y)
        {
            return CombatCellules.Where(cells => cells.pathfindingCell.X == x && cells.pathfindingCell.Y == y).First();
        }
    }
}
