﻿using IronOcr;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheBigBot
{
    public static class MapManager
    {
        public static Color EnnemyColor = Color.FromArgb(46, 54, 61);
        
        public static void GetMapInformations()
        {
            Bitmap bitMap = BitMapGetter.GetCoordonnées();
            AutoOcr OCR = new AutoOcr() { ReadBarCodes = false };
             OcrResult res = OCR.Read(bitMap);
            string[] splittedRes = res.Text.Split('\n');
            MapInformations.ZoneName = splittedRes.First();
            MapInformations.X = int.Parse(splittedRes[2].Split(',').First());
            MapInformations.Y = int.Parse(splittedRes[2].Split(',')[1]);
        }

        public static Pixel GetPixelWithColor(Color color, Bitmap newBitMap = null)
        {
            DirectBitmap dbt;
            if (newBitMap == null)
            {
                Bitmap bitmap = GetScreenBitmap();
                dbt = new DirectBitmap(bitmap.Size.Width, bitmap.Size.Height);
                Graphics graphics = Graphics.FromImage(dbt.Bitmap);
                graphics.DrawImage(bitmap, 0, 0);
            }
            else
            {
                dbt = new DirectBitmap(newBitMap.Size.Width, newBitMap.Size.Height);
                Graphics graphics = Graphics.FromImage(dbt.Bitmap);
                graphics.DrawImage(newBitMap, 0, 0);
            }
            int width = dbt.Width;
            int height = dbt.Height;


            for (int i = 0; i < width; i++)
            {
                for (int n = 0; n < height; n++)
                {  
                    if (dbt.GetPixel(i, n) == color)
                    {
                        dbt.Dispose();
                        return new Pixel(320 + i, n + 23, color);
                    }
                }
            }
            dbt.Dispose();
            return null;
        }

        public static Bitmap GetScreenBitmap()
        {
            Graphics g = Graphics.FromHwnd(IntPtr.Zero);
            Bitmap bitMap = new Bitmap(1280,
                              900,
                              g);
            var gfxScreenshot = Graphics.FromImage(bitMap);

            gfxScreenshot.CopyFromScreen(320,
                            23,
                            0,
                            0,
                            bitMap.Size,
                            CopyPixelOperation.SourceCopy);

            return bitMap;
        }
    }
}
