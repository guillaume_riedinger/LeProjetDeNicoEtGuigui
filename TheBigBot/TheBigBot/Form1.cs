﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheBigBot.Combat;
using TheBigBot.Mining;

namespace TheBigBot
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            WindowsManager.Form = this;
            MouseManager.Form = this;
            txtBoxPm.Text = Properties.Settings.Default.PM.ToString();
            txtBoxSortCount.Text = Properties.Settings.Default.Lances.ToString();
            txtBoxRange.Text = Properties.Settings.Default.Portee.ToString();
            WindowsManager.Log("Bienvenue dans le super bot de nico et guigui !");
        }

        private void btnInit_Click(object sender, EventArgs e)
        {
            WindowsManager.Log("Initialisation ...");
            MineManager.InstantiateMines();
            Process process = Process.GetProcessesByName("dofus")[0];
            process.WaitForInputIdle();
            WindowsManager.SetForegroundWindow(process.MainWindowHandle);

            MapManager.GetMapInformations();

            lblZone.Text = MapInformations.ZoneName;
            lblMap.Text = MapInformations.X + ", " + MapInformations.Y;
            int levelThreshold = 200;
            if (txtBxLevel.Text != "") levelThreshold = int.Parse(txtBxLevel.Text);
            JobManager.StartMining(MineManager.mines[0], checkBxLoopUntilPodsFull.Checked, levelThreshold);

        }

        public void Log(string text)
        {
            txtBoxConsole.AppendText(text + "\r\n");
        }

        public void ChangeScreenImage(Bitmap bitmap)
        {
            pictureBoxScreen.Image = bitmap;
        }

        private void btnStartFight_Click(object sender, EventArgs e)
        {
            CombatManager.InitTurn();
        }

        private void txtBxLevel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnChangePlayerInfos_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.PM = int.Parse(txtBoxPm.Text);
            Properties.Settings.Default.Lances = int.Parse(txtBoxSortCount.Text);
            Properties.Settings.Default.Portee = int.Parse(txtBoxRange.Text);
            Properties.Settings.Default.Save();
        }
    }
}
