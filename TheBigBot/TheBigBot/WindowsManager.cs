﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TheBigBot
{
    public static class WindowsManager
    {
        public static Form1 Form { get; set; }

        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        public static void Log(string text)
        {
            Form.Log(text);
        }

        public static void ChangeScreenImage(Bitmap bitMap)
        {
            Form.ChangeScreenImage(bitMap);
        }
    }
}
