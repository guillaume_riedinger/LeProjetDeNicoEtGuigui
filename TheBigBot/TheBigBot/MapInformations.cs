﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheBigBot
{
    public static class MapInformations
    {
        public static int X { get; set; }
        public static int Y { get; set; }
        public static string ZoneName { get; set; }
    }
}
